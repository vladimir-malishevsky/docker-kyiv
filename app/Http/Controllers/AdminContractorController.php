<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contractor;
use Illuminate\Support\Facades\Redirect;
use PHPUnit\Util\Json;

class AdminContractorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //вывод информации 


        $model_contractor = new Contractor();

        $contractors = $model_contractor->getContractors();

        return new Json($contractors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //информация для внешки создания 

        //$model_contractor = new Contractor();
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //создает по прийдённой информации в $request

        $type = $request->input('type');
        $name = $request->input('name');
        $start_time = $request->input('start_time');
        $end_time = $request->input('end_time');

        $contractor = new Contractor();
        $contractor->type = $type;
        $contractor->name = $name;
        $contractor->start_time = $start_time;
        $contractor->end_time = $end_time;

        $contractor->save();

    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Информация для внешки редактирования 

        $model_contractor = new Contractor();

        $contractor = Contractor::where('id', $id)->first();//////////////////////////////

        return new json($contractor);

       



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)//////////////////////////////////////////////
    {
        //редактирует по прийдённой информации $request

        $contractor = Contractor::where('id', $id)->first();


        $contractor->type = $request->input('type');
        $contractor->name = $request->input('name');
        $contractor->start_time = $request->input('start_time');
        $contractor->end_time = $request->input('end_time');

        $contractor->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //удаляет по id

        Contractor::destroy($id);

    }
}
