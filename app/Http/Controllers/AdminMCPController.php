<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MCP;
use Illuminate\Support\Facades\Redirect;
use PHPUnit\Util\Json;

class AdminMCPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //вывод информации 


        $model_mcp = new MCP();

        $mcps = $model_mcp->getmcps();

        return new Json($mcps);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //информация для внешки создания 

        //$model_mcp = new MCP();
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //создает по прийдённой информации в $request

        $direction_of_activity = $request->input('direction_of_activity');
        $program_activities = $request->input('program_activities');
        $start_deadline = $request->input('start_deadline');
        $end_deadline = $request->input('end_deadline');
        $performers = $request->input('performers');
        $source_of_funding = $request->input('source_of_funding');
        $amounts_of_funding = $request->input('amounts_of_funding');

        $mcp = new MCP();
        $mcp->direction_of_activity = $direction_of_activity;
        $mcp->program_activities = $program_activities;
        $mcp->start_deadline = $start_deadline;
        $mcp->end_deadline = $end_deadline;
        $mcp->performers = $performers;
        $mcp->source_of_funding = $source_of_funding;
        $mcp->amounts_of_funding = $amounts_of_funding;

        $mcp->save();

    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Информация для внешки редактирования 

        $model_mcp = new MCP();

        $mcp = MCP::where('id', $id)->first();//////////////////////////////

        return new json($mcp);

       



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)//////////////////////////////////////////////
    {
        //редактирует по прийдённой информации $request

        $mcp = MCP::where('id', $id)->first();


        $mcp->direction_of_activity = $request->input('direction_of_activity');
        $mcp->program_activities = $request->input('program_activities');
        $mcp->start_deadline = $request->input('start_deadline');
        $mcp->end_deadline = $request->input('end_deadline');
        $mcp->performers = $request->input('performers');
        $mcp->source_of_funding = $request->input('source_of_funding');
        $mcp->amounts_of_funding = $request->input('amounts_of_funding');



        $mcp->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //удаляет по id

        MCP::destroy($id);

    }
}
