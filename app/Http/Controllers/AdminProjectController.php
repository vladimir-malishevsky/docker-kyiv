<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Project_History;
use Illuminate\Support\Facades\Redirect;
use PHPUnit\Util\Json;

class AdminProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //вывод информации 


        $model_project = new Project();

        $projects = $model_project->getProjects();

        return new Json($projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //информация для внешки создания 

        //$model_project = new Project();
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //создает по прийдённой информации в $request

        $official_name = $request->input('official_name');
        $abbreviation = $request->input('abbreviation');

        $project = new Project();
        $project->official_name = $official_name;
        $project->abbreviation = $abbreviation;

        $project->save();


    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Информация для внешки редактирования 

        $model_project = new Project();

        $project = Project::where('id', $id)->first();

        return new json($project);

       



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)//////////////////////////////////////////////
    {
        //редактирует по прийдённой информации $request

        $project = Project::where('id', $id)->first();

        $project->official_name = $request->input('official_name');
        $project->abbreviation = $request->input('abbreviation');

        $project->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($delid)///////////////////////////////////////////////////////////////////
    {
        //удаляет по id

        $project_history = new Project_History();
        $project = Project::where('id', $delid)->first();

        $project_history->id = $project->id;
        $project_history->official_name = $project->official_name;
        $project_history->abbreviation = $project->abbreviation;
        $project_history->comment = $project->comment;
        $project_history->created_at = $project->timestamp('created_at'); 
        $project_history->updated_at = $project->timestamp('updated_at'); 
       
        $project_history->save();

        Project::destroy($delid);

    }
}
