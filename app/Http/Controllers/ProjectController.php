<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\Project;

class ProjectController extends Controller
{

    public function project($id)
    {
        $model_projects = new Project();
        $project = $model_projects->getProjectByID($id);

        return ($project);
    }

    public function list(Request $request)
    {
//        $model_projects = new Project();
//        $projects = $model_projects->getProjects();

//        $projects = Project::all();

        return response()->json('$projects');
        return response()->json($projects);

    }

}
