<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Contractor extends Model
{

        use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contractor';


    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    
    public function getContractorByIDProject($id){

        if(!$id) return null;

        $contractor = DB::table('contractor')
                    ->where('contractor.project_id', '=', $id)
                    ->get()->first();

        return $contractor;
    }

    public function project() {
        return $this->belongsTo(Project::class);
    }








}