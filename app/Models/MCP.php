<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MCP extends Model
{
    //public const SORT_NO_SORT = 1;
   // public const SORT_BY_DATE = 2;
    //public const SORT_BY_DESTINATION = 3;

        use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mcp';


    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public function project() {
        return $this->belongsTo(Project::class);
    }


    
}