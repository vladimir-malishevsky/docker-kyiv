<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Project extends Model
{

        use HasFactory;

        /*
        protected $fillable = [
            'title',
            'description',
            'price',
            'manufacturer_id',
            'count',
        ];
        */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'projects';


    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public function getProjects()
    {
        $projects = DB::table('project')
                    ->get();

        return $projects;
    }

    public function getProjectByID($id){

        if(!$id) return null;

        $project = DB::table('project')
                    ->where('project.id', '=', $id)
                    ->get()->first();

        return $project;
    }

    public function contractors() {
        return $this->hasMany(Contractor::class);
    }

    public function mcps() {
        return $this->hasMany(MCP::class);
    }


}

