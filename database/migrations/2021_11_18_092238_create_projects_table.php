<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Facade\DB;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            //$table->uuid('id')->primary();
           #$table->increments('As_id')->unique();
            //$table->increments('as_id')->unique();
           
            $table->id('id');
            $table->string('official_name');
            $table->string('abbreviation');
            $table->timestamps();

            /*
            $table->string('description');
            $table->string('developer');
            $table->string('admin_center');
            $table->string('admin');
            $table->integer('count');
            $table->string('manager');
            $table->string('comment');
            */
            



            /*
            
            uuid_generate_v5(namespace uuid, name text)
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
