<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMcpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mcp', function (Blueprint $table) {

            //$table->uuid('id')->primary();
            $table->id();
           // $table->foreignId('project_id')->references('id')->on('project')->onUpdate('cascade')->onDelete('cascade');
            $table->string('direction_of_activity');
            $table->string('program_activities');
            $table->integer('start_deadline');
            $table->integer('end_deadline');
            $table->string('performers');
            $table->string('source_of_funding');
            $table->integer('amounts_of_funding');
           // $table->json('expected_result');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mcp');
    }
}
