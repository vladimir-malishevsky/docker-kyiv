<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('project')->insert([
            ['official_name' => 'Text 1', 'abbreviation' => 'Text 1'],
            ['official_name' => 'Text 2', 'abbreviation' => 'Text 2'],
            ['official_name' => 'Text 3', 'abbreviation' => 'Text 3'],
        ]);
        
        
    }
}
