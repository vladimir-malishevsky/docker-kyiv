FROM php:7.4-fpm

#Указываем рабочую директорию
WORKDIR /var/www/lumen

#Обновляем список репозиториев и устанавливаем расширения PDO для php
RUN apt-get update 


RUN apt-get install -y libpq-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql 

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
 

    
    #
  


#RUN docker-php-ext-install pdo 
#RUN docker-php-ext-install pdo_pgsql
