<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use \App\Http\Controllers\ProjectController;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*
$router->get('/', function () use ($router) {
    return response()->json(app('db')->select("SELECT official_name FROM project"));
    //return response()->json();
});
*/

//$router->get('/', function (){echo 'lol';});
$router->get('/', [ProjectController::class, 'list']);
$router->get('project/{project_number}', [ProjectController::class, 'project']);


/*
CREATE TABLE sal_emp (
    name            text,
    pay_by_quarter  integer[],
    schedule        text[][]
);

*******************************

INSERT INTO sal_emp
    VALUES ('Bill',
    '{10000, 10000, 10000, 10000}',
    '{{"meeting", "lunch"}, {"training", "presentation"}}');
*/

//$router->
//('admin/projects', AdminProjectController::class);

